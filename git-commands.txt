Generic GIT Commands:

For New Repository:
git init
git add README.md or git add .
git commit -m "first commit"
git remote add origin git@github.com:mavrick202/hello123.git
For any email error
git config --global user.email "you@example.com"
git config --local -l
git push -u origin master

For existing Repository:
rm -rf .git
git init
git remote add origin git@github.com:mavrick202/hello123.git
git push -u origin master

git add . or git reset -- HARD . /git reset HEAD -- testfile4
git commit
git reset --hard HEAD
git reset --hard commitid
git clean -n
git clean -d -f
git commit --amend
git blame filename
git push --force 

git config --global core.editor "nano"
git branch
git checkout -b branch
git branch -D
git stash
git stash save "add style to our site"
git stash list
git stash pop stash@{2}
git merge
git rebase
git rebase -i HEAD~n
git squash


git secrets
git tags 
git cherry-pick
